

var list_parth = new Array ;

var str = "" ;
$( 'textarea#enter_zone' ).val( '' ) ;
$( 'button.but' ).click( function(){
    if( $( this ).text() !== "=" )
    {
        $( 'textarea#enter_zone' ).val( $( 'textarea#enter_zone' ).val() + $( this ).text() ) ;
    }
    if( $( this ).text() === "=" )
    {
        str = $( 'textarea#enter_zone' ).val() ;

        $( 'div#result_zone' ).html( $( 'div#result_zone' ).html() + str + ' = ' + basic_calc( str )[ 0 ] + '<br/>' ) ;

        $( 'textarea#enter_zone' ).val( '' ) ;
    }
});

/*var s = '((3+4)*(5+6)-(4+2))-(4+6)+(4+3)' ;
var tab = decoup_parth( s ) ;
for( i = 0 ; i < tab.length ; i ++ )
{
    console.log( tab[ i ] ) ;
}*/

function verf_string_valid( str )
{
    var n_p_o = 0 ;
    var n_p_f = 0 ;
    var cpt = 0 ;
    var verf = 0 ;
    for( cpt = 0 ; cpt < str.length ; cpt ++ )
    {
        if( str[ cpt ] === '(' )
            n_p_o ++ ;
        else if( str[ cpt ] === ')' )
            n_p_f ++ ;
    }

    if( n_p_o === n_p_f )
        verf = 1 ;

    return verf ;
}

function verf_parth_exist( str )
{
    var cpt = 0 ;
    var verf = 0 ;
    for( cpt = 0 ; cpt < str.length ; cpt ++ )
    {
        if( str[ cpt ] === '(' )
            verf = 1 ;
    }

    return verf ;
}

function decoup_parth( str )
{
    var tab = new Array ;
    var str1 = '' ;
    var str2 = '' ;
    var cpt = 0 ;
    var cpt1 = 0 ;
    var cpt2 = 0 ;
    var n = 0 ;
    var n_p_o = 0 , n_p_f = 0 ;
    for( cpt = 0 ; cpt < str.length ; cpt ++ )
    {
        if( str[ cpt ] !== '(' )
        {
            str1 = str1 + str[ cpt ] ;
            if( cpt === str.length - 1 )
            {
                tab[ cpt1 ] = str1 ;
            }
        }
        else if( str[ cpt ] === '(' )
        {
            if( str1 !== '' )
            {
                tab[ cpt1 ] = str1 ;
                cpt1 ++ ;
                str1 = '' ;
            }
            
            for( cpt2 = cpt ; cpt2 < str.length ; cpt2 ++ )
            {
                if( str[ cpt2 ] === '(' )
                    n_p_o ++ ;
                else if( str[ cpt2 ] === ')' )
                    n_p_f ++ ;

                str1 = str1 + str[ cpt2 ] ;
                if( n_p_o === n_p_f )
                {
                    for( n = 1 ; n < str1.length - 1 ; n ++ )
                    {
                        str2 = str2 + str1[ n ] ;
                    }
                    tab[ cpt1 ] = str2 ;
                    str1 = '' ;
                    str2 = '' ;
                    cpt1 ++ ;
                    cpt = cpt2 ;
                    cpt2 = str.length ;
                    n_p_o = 0 ;
                    n_p_f = 0 ;
                }
            }
        }
    }
    return tab ;
}

//console.log( calc_advanced( 'sin3' ) ) ;
function calc_advanced( str )
{
    var number = Number( str ) ;
    //console.log( str ) ;
    var s_str1 = '' , s_str2 = '' , str3 = '' ;
    var c = 0 , d = 0 , e = 0 ;
    for( c = str.length - 1 ; c >= 0 ; c -- )
    {
        //str3 = Number( str[ c ] ) ;
        //console.log( str[ c ] ) ;
        if( !Number( str[ c ] ) && e === 0 && Number( str[ c ] ) !=0 & str[ c ] !== '.' && str[ c ] !== '-' && str[ c ] !== '+' )
        {
            for( d = c + 1 ; d < str.length ; d ++ )
            {
                s_str1 = s_str1 + str[ d ] ;
            }
            number = Number( s_str1 ) ;
            e = 1 ;
            c ++ ;
        }
        else
        {
            if( str[ c ] === 's' )
            {
                s_str2 = s_str2 + str[ c ] + str[ c+1 ] + str[ c+2 ] ;
                if( s_str2 === 'sin' )
                {
                    number = Math.sin( number ) ;
                }
                s_str2 = '' ;
            }

            else if( str[ c ] === 'c' )
            {
                s_str2 = s_str2 + str[ c ] + str[ c+1 ] + str[ c+2 ] ;
                if( s_str2 === 'cos' )
                {
                    number = Math.cos( number ) ;
                }
                else if( s_str2 === 'cot' )
                {
                    number = Math.cos( number ) / Math.sin( number ) ;
                }
                s_str2 = '' ;
            }

            else if( str[ c ] === 't' )
            {
                s_str2 = s_str2 + str[ c ] + str[ c+1 ] + str[ c+2 ] ;
                if( s_str2 === 'tan' )
                {
                    number = Math.tan( number ) ;
                }
                s_str2 = '' ;
            }

            else if( str[ c ] === 'l' )
            {
                s_str2 = s_str2 + str[ c ] + str[ c+1 ] + str[ c+2 ] ;
                if( s_str2 === 'log' )
                {
                    number = Math.log( number ) ;
                }
                else
                {
                    number = Math.ln( number ) ;
                }
                s_str2 = '' ;
            }

            else if( str[ c ] === '^' )
            {
                number = Math.pow( Number( str[ c -1 ] ) , number ) ;
            }
            else if( str[ c ] === '√' )
            {
                number = Math.sqrt( number ) ;
            }
            else if( str[ c ] === 'e' )
            {
                number = Math.exp( number ) ;
            }
            else if( str[ c ] === '!' )
            {
                var ft = 1 ;
                if( number > 0 )
                {
                    for( var t = number ; t > 0 ; t -- )
                    {
                        ft = ft * t ;
                    }
                    number = ft ;
                }

                else if( number < 0 )
                {
                    var ft = 1 ;
                    for( var t = number ; t < 0 ; t ++ )
                    {
                        ft = ft * t ;
                    }
                    number = ft ;
                }
                else
                    number = 1 ;
            }
        }
    }

    return number ;
}
//console.log( Number( '+3' ) ) ;
function splite_plus( str )
{
    var cpt = 0 ;
    var ad = 0 ;
    var ae = 0 ;
    var tab = new Array ;
    tab[ 0 ] = '' ;

    for( cpt = 0 ; cpt < str.length ; cpt++ )
    {
        if( str[ cpt ] !== '+' )
        {
            tab[ ad ] = tab[ ad ] + str[ cpt ] ;
        }
        else if( str[ cpt ] === '+' && ( str[ cpt -1 ] !== '/' && str[ cpt -1 ] !== '*' ) )
        {
            ad ++ ;
            tab[ ad ] = '' ;
        }
        else if( str[ cpt ] === '+' && ( str[ cpt -1 ] === '/' || str[ cpt -1 ] === '*' ) )
        {
            tab[ ad ] = tab[ ad ] + str[ cpt ] ;
            tab[ ad ] = tab[ ad ] + str[ cpt + 1 ] ;
            ad++ ;
            tab[ ad ] = '' ;
            cpt++ ;
        }

    }
    
    return tab ;
}

function splite_moins( str )
{
    var cpt = 0 ;
    var ad = 0 ;
    var ae = 0 ;
    var tab = new Array ;
    var tab1 = new Array ;
    tab[ 0 ] = '' ;

    for( cpt = 0 ; cpt < str.length ; cpt++ )
    {
        if( str[ cpt ] !== '-' )
        {
            tab[ ad ] = tab[ ad ] + str[ cpt ] ;
        }
        else if( str[ cpt ] === '-' && ( str[ cpt -1 ] !== '/' && str[ cpt -1 ] !== '*' ) )
        {
            ad ++ ;
            tab[ ad ] = '' ;
        }
        else if( str[ cpt ] === '-' && ( str[ cpt -1 ] === '/' || str[ cpt -1 ] === '*' ) )
        {
            tab[ ad ] = tab[ ad ] + str[ cpt ] ;
            tab[ ad ] = tab[ ad ] + str[ cpt + 1 ] ;
            ad++ ;
            tab[ ad ] = '' ;
            cpt++ ;
        }

    }

    ad = 0 ;
    ae = 0 ;
    for( cpt = 0 ; cpt < tab.length ; cpt ++ )
    {
        if( tab[ cpt ][ 0 ] != '/' || tab[ cpt ][ 0 ] != '*' )
        {
            console.log( tab[ cpt ] ) ;
            tab1[ ad ] = tab[ cpt ] ;
            ad++ ;
            ae = cpt ;
        }

        else if( tab[ cpt ][ 0 ] == '/' || tab[ cpt ][ 0 ] == '*' )
        {
            console.log( tab[ cpt - 1 ] ) ;
            tab1[ ad -1 ] = tab1[ ad -1 ] + tab[ cpt ] ;
        }
    }
    console.log( tab ) ;
    return tab1 ;
}

function basic_calc( str )
{
    var tab_plus = new Array ;
    var tab_moin = new Array ;
    var tab_div = new Array ;
    var tab_mult = new Array ;

    var tab_pos = new Array ;
    var tab_pos1 = new Array ;
    var tab_pos2 = new Array ;

    var i = 0 , j = 0 , k = 0 , l = 0 , cm = 1 ;
    
    tab_moin = [] ;
    tab_plus = [] ;
    tab_div = [] ;
    tab_mult = [] ;
    tab_pos = [] ;
    j = 0 , k = 0 ;
    tab_plus[ j ] = splite_plus( str ) ;
        for( i = 0 ; i < tab_plus[ j ].length ; i ++ )
        {
            tab_moin[ k ] = splite_moins( tab_plus[ j ][ i ] ) ;
            tab_pos2[ k ] = new Array ;
            tab_pos2[ k ][ 0 ] = i ;
            tab_pos2[ k ][ 1 ] = j ;
            k++ ;
        }

        k = 0 ;

        for( i = 0 ; i < tab_moin.length ; i ++ )
        {
            for( j = 0 ; j < tab_moin[ i ].length ; j++ )
            {
                tab_mult[ k ] = tab_moin[ i ][ j ].split( '*' ) ;
                tab_pos1[ k ] = new Array ;
                tab_pos1[ k ][ 0 ] = i ;
                tab_pos1[ k ][ 1 ] = j ;
                k++ ;
            }

        }

        k = 0 ;

        for( i = 0 ; i < tab_mult.length ; i ++ )
        {
            for( j = 0 ; j < tab_mult[ i ].length ; j++ )
            {
                tab_div[ k ] = tab_mult[ i ][ j ].split( '/' ) ;
                tab_pos[ k ] = new Array ;
                tab_pos[ k ][ 0 ] = i ;
                tab_pos[ k ][ 1 ] = j ;
                k++ ;
            }

        }

        for( i = 0 ; i < tab_div.length ; i++ )
        {
            if( tab_div[ i ].length == 1 )
            {
                tab_div[ i ][ 0 ] = calc_advanced( tab_div[ i ][ 0 ] ) ;
            }
            if( tab_div[ i ].length > 1 )
            {
                for( j = 0 ; j < tab_div[ i ].length ; j ++ )
                {
                    tab_div[ i ][ j ] = calc_advanced( tab_div[ i ][ j ] ) ;
                }
                cm = tab_div[ i ][ 0 ]  *  tab_div[ i ][ 0 ] ;
                for( j = 0 ; j < tab_div[ i ].length ; j ++ )
                {
                    cm /= tab_div[ i ][ j ] ;
                }
                //console.log( cm ) ;
                tab_div[ i ] = cm ;
            }
        }

        for( i = 0 ; i < tab_div.length ; i ++ )
        {
            tab_mult[ tab_pos[ i ][ 0 ] ][ tab_pos[ i ][ 1 ] ] = tab_div[ i ] ;
        }

        for( i = 0 ; i < tab_mult.length ; i++ )
        {
            //console.log( tab_mult[ i ] ) ;
            if( tab_mult[ i ].length > 1 )
            {
                cm = 1 ;
                for( j = 0 ; j < tab_mult[ i ].length ; j ++ )
                {
                     cm *= Number( tab_mult[ i ][ j ] ) ;
                }
                tab_mult[ i ] = cm ; 
            }
        }

        for( i = 0 ; i < tab_mult.length ; i ++ )
        {
            tab_moin[ tab_pos1[ i ][ 0 ] ][ tab_pos1[ i ][ 1 ] ] = tab_mult[ i ] ;
        }

        for( i = 0 ; i < tab_moin.length ; i++ )
        {
            if( tab_moin[ i ].length > 1 )
            {
                cm = Number( tab_moin[ i ][ 0 ] ) * 2 ;
                for( j = 0 ; j < tab_moin[ i ].length ; j ++ )
                {
                    cm -= Number( tab_moin[ i ][ j ] ) ;
                }
                tab_moin[ i ] = cm ;
            }
        }

        if( tab_moin.length > tab_plus.length )
        {
            for( i = 0 ; i < tab_moin.length ; i ++ )
            {
                tab_plus[ 0 ][ i ] = tab_moin[ i ] ;
            }
        }

        else
        {
           for( i = 0 ; i < tab_moin.length ; i ++ )
            {
                tab_plus[ tab_pos2[ i ][ 0 ] ][ tab_pos2[ i ][ 1 ] ] = tab_moin[ i ] ;
            }
        }
 

        for( i = 0 ; i < tab_plus.length ; i++ )
        {
            if( i  === 0 )
            {
                cm = 0 ;
                for( j = 0 ; j < tab_plus[ i ].length ; j ++ )
                {
                    cm += Number( tab_plus[ i ][ j ] ) ;
                }
                tab_plus[ i ] = cm ;
            }
        }

        return tab_plus ;
}